Post til node med følgende json body:
	
{
	"memeName" : "One does not simply",
	"memeHeader" : "One does not simply",
	"memeFooter" : "Use imgflip with postman and node"
}

MemeName referer til navnet på det meme der vil anvendes. Header og footer er så selvfølgelig teksten dertil.

Programmet anvender imgflips api til at generere memes: https://api.imgflip.com/