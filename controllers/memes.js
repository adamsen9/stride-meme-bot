module.exports.controller = function (app) {
    let axios = require('axios');
    let FormData = require('form-data');
    let concat = require("concat-stream")
    let FuzzySet = require('fuzzyset.js');

    let fuzzyMemeSet;
    let memes = {};

    //controller setup
    axios.get('https://api.imgflip.com/get_memes')
        .then(r => {
            r.data.data.memes.map(a => memes[a.name] = {
                id: a.id
            })
            fuzzyMemeSet = new FuzzySet(r.data.data.memes.map(a => a.name));
        }).catch(function (error) {
            console.log(error);
        })


    app.get('/memes', function (req, res) {
        axios.get('https://api.imgflip.com/get_memes')
            .then(r => {
                if (r.data.success) {
                    res.send({
                        memes: r.data.data.memes.map(a => a.name)
                    });
                } else {
                    res.send({
                        error_message: r.data.error_message
                    })
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    })

    app.post('/memes', function (req, res) {
        let requestBody = req.body;

        let fuzzyLookupResult = fuzzyMemeSet.get(requestBody.memeName);

        if (fuzzyLookupResult === []) {
            res.send({
                message: 'no response'
            })
            return;
        } else {

            let memeId = memes[fuzzyLookupResult[0][1]].id;
            let imglipUsername = "throwaway12345";
            let imgflipPassword = "Abcd_1234";

            let formData = new FormData();

            formData.append('template_id', memeId);
            formData.append('username', imglipUsername);
            formData.append('password', imgflipPassword);
            formData.append('text0', requestBody.memeHeader);
            formData.append('text1', requestBody.memeFooter);

            formData.pipe(concat(data => {
                axios.post('https://api.imgflip.com/caption_image', data, {
                        headers: formData.getHeaders()
                    }).then(r => {
                        if (r.data.success) {
                            res.send({
                                url: r.data.data.url
                            });
                        } else {
                            res.send({
                                error_message: r.data.error_message
                            })
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }))
        }
    })
}