var express = require('express');
var http = require('http');
var path = require('path');
var app = express();
var fs = require('fs');
var axios = require('axios');
var bodyParser = require('body-parser');

app.use(bodyParser.json())

// dynamically include routes (Controller)
fs.readdirSync('./controllers').forEach(function (file) {
  if(file.substr(-3) == '.js') {
      route = require('./controllers/' + file);
      route.controller(app);
  }
});

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'

app.listen(server_port, server_ip_address, () => {
    console.log('Server started!');
})